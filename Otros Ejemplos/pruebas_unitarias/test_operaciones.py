import unittest
import operaciones

class TestOperaciones(unittest.TestCase):

    def test_sumar(self):
        self.assertEqual(operaciones.sumar(2,3),5)
        self.assertEqual(operaciones.sumar(1000,2000),3000)

    def test_restar(self):
        self.assertEqual(operaciones.restar(5,2),-3)


  
       