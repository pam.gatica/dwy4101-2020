function validar(){
    var txtUsuario = document.getElementById("txtUsername");
    var username = txtUsuario.value;

    if(username.length < 5){
        //alert("Nombre de usuario no cumple con formato")
        $("#eUsername").html("Nombre de usuario no cumple con formato");
        return false;
    }

    var pw1 = $("#txtPassword").val();
    var pw2 = $("#txtPassword2").val();

    if(pw1 != pw2){
        $("#ePassword2").html("Las contraseñas no coinciden");
        return false;
    }

    


    return true;
}